import http from './index'
import { Message } from 'element-ui'
//获取权限菜单列表
export const getMenus = async (route) => {
    const res = await http('menus','GET',{},{},route)
    return res.data
}
