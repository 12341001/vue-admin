import axios from 'axios';
import { Message } from 'element-ui';
import router from '@/router'
const baseURL = 'http://localhost:8889/api/private/v1/'

// 添加请求拦截器
axios.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    return config;
  }, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  });

// 添加响应拦截器
axios.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    if(response.data.meta.msg === '无效token'){
        //判断用户修改token时触发
        Message.error(response.data.meta.msg)
        router.push({'name':'login',query:{
            redirect:router.history.current.name
        }})
        window.localStorage.removeItem('vuex')
    }
    return response;
  }, function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
  });

function http(url,method='GET',params={},data={}){ //axios封装
    return axios({
            baseURL,
            url,
            method,
            params,
            data,
            headers:{ Authorization: this.$store.state.usermsg.token }
        }).then(res=>{
            if(res.status >= 200 && res.status < 300 || res.status === 304){
                if(res.data.meta.status >= 200 && res.data.meta.status < 300 ){
                    if(url == 'login'){
                        Message.success(res.data.meta.msg);
                    }
                    return res.data;
                }
                else{
                    Message.error(res.data.meta.msg)
                    Promise.reject(res)
                }
            }else{
                Message.error(res.statusText);
                Promise.reject(res.data.meta.msg)
            }
        }).catch(err=>{
             Promise.reject(err);
        })
    }
export default http;