import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
import dayjs from "dayjs"
Vue.prototype.dayjs = dayjs
import './plugins/element.js'
import 'reset-css'
import http from '@/axios'
Vue.prototype.$http = http
Vue.config.productionTip = false
import echarts from "echarts";
import VueQuillEditor from 'vue-quill-editor'

import 'quill/dist/quill.core.css' // import styles
import 'quill/dist/quill.snow.css' // for snow theme
import 'quill/dist/quill.bubble.css' // for bubble theme

Vue.use(VueQuillEditor)
Vue.prototype.$echarts = echarts;
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
