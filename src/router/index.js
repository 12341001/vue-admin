import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import login from "../views/login.vue";
import store from "../store/index";
import users from "../views/users.vue";
import roles from "../views/roles.vue";
import rights from "../views/rights.vue";
import goods from "../views/goods.vue";
import params from "../views/params.vue";
import categories from "../views/categories.vue";
import orders from "../views/orders.vue";
import reports from "../views/reports.vue";
import addgoods from '../views/addgoods.vue'
Vue.use(VueRouter);
const originalPush = VueRouter.prototype.push

VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
const routes = [
    {
        path: "/",
        redirect: '/Home/users',
    },
    {
        path: "/Home",
        name: "Home",
        redirect:'/Home/users',
        component: Home,
        children:[
            {
                path:'',
                component:users
            },
            {
                path:'users',
                name:'users',
                meta:{
                    title:'用户列表',
                },
                component:users
            },
            {
                path:"roles",
                name:'roles',
                meta:{
                    title:'角色列表',
                },
                component:roles,
            },
            {
                path:"rights",
                name:'rights',
                meta:{
                    title:'权限列表',
                },
                component:rights,
            },
            {
                path:"goods",
                name:'goods',
                meta:{
                    title:'商品列表',
                },
                component:goods,
            },
            {
                path : 'addgoods',
                name:'addgoods',
                meta:{
                    title:'添加商品',
                },
                component:addgoods,
            },
            {
                path:"params",
                name:'params',
                meta:{
                    title:'分类参数',
                },
                component:params,
            },
            {
                path:"categories",
                name:'categories',
                meta:{
                    title:'商品分类',
                },
                component:categories,
            },
            {
                path:"orders",
                name:'orders',
                meta:{
                    title:'订单列表',
                },
                component:orders,
            },
            {
                path:"reports",
                name:'reports',
                meta:{
                    title:'数据报表',
                },
                component:reports,
            },
        ]
    },
    {
        path: "/login",
        name: "login",
        component: login,
    },
];

const router = new VueRouter({
    routes,
});
router.beforeEach((to, from, next) => {
    if (to.name !== "login" && !store.state.usermsg.token)
        next({
            name: "login",
            query: {
                redirect: to.name,
            },
        });
    else next();
});
export default router;
