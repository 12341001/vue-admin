import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate"
Vue.use(Vuex);
export default new Vuex.Store({
    state: {
        usermsg: "",//用户信息
        permsg:'',//权限菜单列表,
        mbxname:'用户管理',//面包屑导航
        roles:[],//
    },
    mutations: {
        mymsg(state, data) {//用户信息
            state.usermsg = data;
        },
        perdat(state,data){//权限菜单
            state.permsg = data
        },
        mbxname(state,data){//面包屑name
            state.mbxname = data
        },
        rolesdat(state,data){//添加角色
            state.roles = data
        }
    },
    actions: {
        per(app,data){//权限菜单
            data.$http("menus", "GET", {}, {}).then((res) => {
                app.commit('perdat',res.data)
            });
        },
        myroles(app,data){//用户列表
            data.$http("roles", "GET").then((res) => {
                app.commit('rolesdat',res.data)
            });
        }
        
    },
    modules: {},
    plugins: [createPersistedState({
        data:window.localStorage
    })]
});
